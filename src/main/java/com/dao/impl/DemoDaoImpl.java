package com.dao.impl;

import com.dao.DemoDao;
import com.entity.Demo;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DemoDaoImpl implements DemoDao {


    private SessionFactory sessionFactory;

    private Session session;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
        session = this.sessionFactory.openSession();
    }

    @Override
    public void save(Demo demo) {
        session.save(demo);
    }

    @Override
    public void update(Demo demo) {
        session.update(demo);
    }

    @Override
    public Demo findById(int id) {
        return session.find(Demo.class, id);
    }

    @Override
    public void delete(Demo demo) {
        session.delete(demo);
    }

    @Override
    public List<Demo> list() {
        return session.createQuery("from Demo", Demo.class).list();
    }
}
