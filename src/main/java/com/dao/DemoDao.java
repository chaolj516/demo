package com.dao;

import com.entity.Demo;

import java.util.List;

public interface DemoDao {

    void save(Demo demo);

    void update(Demo demo);

    Demo findById(int id);

    void delete(Demo demo);

    List<Demo> list();
}
