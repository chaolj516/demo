package com.service.impl;

import com.dao.DemoDao;
import com.entity.Demo;
import com.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DemoServiceImpl implements DemoService {

    @Autowired
    private DemoDao demoDao;

    @Override
    public void save(Demo demo) {
        demoDao.save(demo);
    }

    @Override
    public void update(Demo demo) {
        demoDao.update(demo);
    }

    @Override
    public Demo findById(int id) {
        return demoDao.findById(id);
    }

    @Override
    public void delete(Demo demo) {
        demoDao.delete(demo);
    }

    @Override
    public List<Demo> list() {
        return demoDao.list();
    }
}
