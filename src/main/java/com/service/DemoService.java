package com.service;

import com.entity.Demo;

import java.util.List;

public interface DemoService {

    void save(Demo demo);

    void update(Demo demo);

    Demo findById(int id);

    void delete(Demo demo);

    List<Demo> list();
}
