package com.controller;

import com.entity.Demo;
import com.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/demo")
public class DemoController {
    @Autowired
    private DemoService demoService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView get() {
        ModelAndView view = new ModelAndView("/demo");
        view.addObject("demos", demoService.list());
        return view;
    }

    @RequestMapping(value = "/add",method = RequestMethod.POST)
    public ModelAndView add(@ModelAttribute Demo demo) {
        ModelAndView view = new ModelAndView("redirect:/demo");
        demoService.save(demo);
        view.addObject("message", "success");
        return view;
    }
}
