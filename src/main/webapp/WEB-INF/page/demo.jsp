<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/20 0020
  Time: 上午 11:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Demo</title>
</head>
<body>
    <h1>Hello, World!</h1>
    <table>
        <thead>
            <tr>
                <th>a</th>
                <th>b</th>
                <th>c</th>
                <th>d</th>
            </tr>
        </thead>
        <tbody>
        <c:forEach items="${demos}" var="item">
            <tr>
                <td>${item.a}</td>
                <td>${item.b}</td>
                <td>${item.c}</td>
                <td>${item.d}</td>
            </tr>
        </c:forEach>        </tbody>
    </table>



    <form method="post" action="/demo/add">

        <ul> Test for transaction
            <li>b <input type="text" name="b"></li>
            <li>c <input type="text" name="c"></li>
            <li>d <input type="text" name="d"></li>
        </ul>
        <button type="submit">Create Demo</button>
    </form>
</body>
</html>
